#######################################################################################################################
# Combines outputs from model selection runs to create plots and identify best model
# 
# Objective:  Plot output of looped model selection          
#
# Author:     Katie Gale
#             
# Date:       Dec 11, 2018
######################################################################################################################

library(ggplot2)

outdir<-"../Models/2018-11-28_PMECS/"

#--------------------------------#
# Bring in "fullResults" outputs from different runs (different n RCPs, w/ and w/o sampling effect)
#--------------------------------#
# No survey
r1<-read.csv(paste0(outdir,"/LoopModelSelection_Results/results_600sites_2,3,13,14vars_5RCPs_2018.11.29.csv"))
r2<-read.csv(paste0(outdir,"/LoopModelSelection_Results_run2/results_600sites_13,14vars_4,5-8RCPs_2018.12.04_fullResults.csv"))
r3<-read.csv(paste0(outdir,"/LoopModelSelection_Results_run3/results_600sites_2,3vars_4,5-8RCPs_withSurv_2018.12.04_fullResults.csv"))

nosurv<-rbind(r1,r2,r3)
nosurv<-nosurv[,c(-1)]
nosurv$type<-"nosurv"

sampSize<-data.frame(table(nosurv$nRCP))

# with Survey
s1<-read.csv("./LoopModelSelection_Results_run4_withSurv/results_600sites_2,3,10,11vars_3-5RCPs_withSurv_2018.09.18_fullResults.csv")
s2<-read.csv("./LoopModelSelection_Results_run3_withSurv/results_600sites_2,3,7,8vars_6-9RCPs_withSurv_2018.09.14_fullResults.csv")

surv<-rbind(s1,s2)
surv<-nosurv[,c(-1)]
nosurv$type<-"surv"

sampSize2<-data.frame(table(surv$nRCP))

comb<-rbind(nosurv,surv)
comb<-comb[!is.infinite(comb$minBIC),]
comb$nRCP<-as.factor(comb$nRCP)

best <- comb[which.min(comb$minBIC[!is.infinite(comb$minBIC)]),]

write.csv(comb,paste0(outdir,"./LoopModelSelection_CombineResults/BIC_Table_all_2018.12.06.csv" ))

#--------------------------------#
#Plot BIC by nRCPs 
#--------------------------------#
png(paste0(outdir,"./LoopModelSelection_CombineResults/Formula_BICs_withsurv_all.png"), res=200, height=12,width=12, units="cm")
par(mar=c(4,4,1,1))
plot(comb$nRCP[comb$type=="nosurv"], comb$minBIC[comb$type=="nosurv"],  xlab="n RCPs",  ylab="min BIC for formula", col=rgb(.1,.2,.8,.3), border="darkblue",ylim=c(5100,max(comb$minBIC)))
par(new=TRUE)
plot(comb$nRCP[comb$type=="surv"], comb$minBIC[comb$type=="surv"],  xlab="n RCPs",  ylab="min BIC for formula", border="darkred",col=rgb(.8,.6,.5,.3), ylim=c(5100,max(comb$minBIC)))
text(x=c(1:nrow(sampSize)),y=25185,labels = c(paste0("n=",sampSize$Freq[1]),sampSize$Freq[2:nrow(sampSize)]), col="darkblue")
text(x=c(1:nrow(sampSize2)),y=25090,labels = sampSize2$Freq, col="darkred")
legend("topright",legend=c("No covariate","Survey as covariate"),pch=22, pt.bg=c(rgb(.1,.2,.8,.3),rgb(.8,.6,.5,.3)), col=c("darkblue","darkred"), bty="n")
dev.off()


#--------------------------------#
#get contribution of each individual variable
#--------------------------------#
d1<-read.csv(paste0(outdir,"/LoopModelSelection_Results/results_600sites_2,3,13,14vars_5RCPs_2018.11.29_Drop1results.csv"))
d2<-read.csv(paste0(outdir,"/LoopModelSelection_Results_run2/results_600sites_13,14vars_4,5-8CPs_2018.12.04_drop1Results.csv"))


drop<-rbind(d1,d2)
drop<-drop[,c(-1)]
drop<-drop[drop$whichMissing!=0,]
drop$whichMissing<-droplevels(drop$whichMissing)
drop$surv<-"nosurv"

ds1<-read.csv("./LoopModelSelection_Results_run3_withSurv/results_600sites_2,3,7,8vars_6-9RCPs_withSurv_2018.09.14_drop1Results.csv")
ds2<-read.csv("./LoopModelSelection_Results_run4_withSurv/results_600sites_2,3,10,11vars_3-5RCPs_withSurv_2018.09.18_drop1Results.csv")

dropSurv<-rbind(ds1,ds1)
dropSurv<-dropSurv[dropSurv$whichMissing!=0,]
dropSurv$whichMissing<-droplevels(dropSurv$whichMissing)
dropSurv$surv<-"surv"

dropBoth<-drop
dropBoth<-rbind(drop, dropSurv)
dropBoth$whichMissing<-gsub("1","",dropBoth$whichMissing)

#--------------------------------#
# Plot change in model fit for each variable, considering all RCPs together
#--------------------------------#
png(paste0(outdir,"/LoopModelSelection_CombineResults/VariableContribution_survnosurv.png"), res=200, height=15,width=15, units="cm", pointsize=10)
par(mar=c(11,5,1,1))
plot(as.factor(dropBoth$whichMissing[dropBoth$surv=="nosurv"]), dropBoth$deltaBIC[dropBoth$surv=="nosurv"], las=2, ylab=c("Change in BIC from full model,\naveraged across nRCP iterations"), border="darkblue",col=rgb(.1,.2,.8,.3), ylim=c(min(dropBoth$deltaBIC), max(dropBoth$deltaBIC)))
par(new=T)
plot(as.factor(dropBoth$whichMissing[dropBoth$surv=="surv"]), dropBoth$deltaBIC[dropBoth$surv=="surv"], las=2, ylab=c("Change in BIC from full model,\naveraged across nRCP iterations"), border="darkred",col=rgb(.8,.6,.5,.3),ylim=c(min(dropBoth$deltaBIC), max(dropBoth$deltaBIC)))
abline(h=0)
legend("topleft",legend=c("No covariate","Survey as covariate"),pch=22, pt.bg=c(rgb(.1,.2,.8,.3),rgb(.8,.6,.5,.3)), col=c("darkblue","darkred"), bty="n")
dev.off()

#--------------------------------#
# Plot change in model fit for each variable, separating out RCPs
#--------------------------------#
png("./CombineResults/VariableContribution_byRCP_nosurv.png", res=350, height=15,width=25, units="cm")
par(mar=c(3,3,1,1))
ggplot(data=drop, aes(x=nRCP,y=deltaBIC, col=whichMissing))+
  geom_line()+geom_point(aes(shape=whichMissing))+theme_bw()+
  scale_color_manual(values=c(  "#33ccff","#0033cc","#66ff66","#009900","#fb9a99","#e31a1c","#9933ff","#ff7f00","#cab2d6","#6a3d9a","#663300"))+
  scale_shape_manual(values=c(1:6, 15:19))+ylab(c("Change in BIC from Full Model for each nRCP"))+
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank())+
  geom_hline(yintercept = 0, linetype=3)
dev.off()

